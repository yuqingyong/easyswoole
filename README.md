# 微聊

EASYSWOOLE 聊天室DEMO

## 在线体验

[在线DEMO](http://swoole.yuqingyong.cn:9501/)

## 安装

安装时遇到提示是否覆盖 `EasySwooleEvent.php` 请选择否 (输入 n 回车)

```bash
git clone https://gitee.com/yuqingyong/easyswoole.git
composer install
php vendor/easyswoole install
```

## 配置

修改 `dev.php` 内的配置项，改为自己服务器的信息

```ini
'HOST' => 'http://127.0.0.1:9501',
'WEBSOCKET_HOST' => 'ws://127.0.0.1:9501',
```

## 启动

```bash
php easyswoole start
```
