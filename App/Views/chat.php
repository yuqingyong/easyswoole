<!doctype html>
<html lang="zh-Hans">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>私聊页面</title>
    <link rel="stylesheet" href="https://cdn.staticfile.org/amazeui/2.7.2/css/amazeui.min.css">
    <link rel="stylesheet" href="https://cdn.staticfile.org/layer/2.3/skin/layer.css">
    <link rel="stylesheet" href="/css/main.css?v=120203">
    <script src="https://cdn.staticfile.org/vue/2.5.17-beta.0/vue.js"></script>
    <script src="https://cdn.staticfile.org/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdn.staticfile.org/layer/2.3/layer.js"></script>
    <script src="https://cdn.bootcss.com/jquery/3.4.1/jquery.min.js"></script>
</head>

<body>
<div id="chat">
    <template>
        <div class="online_window">
            <img src="images/chat_bg.png" style="width: 250px;height: 650px;" alt="">
        </div>
        <div class="talk_window">
            <div class="windows_top">
                <div class="windows_top_left"><i class="am-icon am-icon-list online-list"></i> {{ param_data.username }}</div>
                <div class="windows_top_right">
                    <a href="http://yqy.ndstop.com" target="_blank"
                       style="color: #999">星辰网络博客</a>
                </div>
            </div>
            <div class="windows_body" id="chat-window" v-scroll-bottom>
                <ul class="am-comments-list am-comments-list-flip">
                    <template v-for="chat in roomChat">
                        <template v-if="chat.type === 'tips'">
                            <div class="chat-tips">
                                <span class="am-badge am-badge-primary am-radius">{{chat.content}}</span></div>
                        </template>
                        <template v-else>
                            <div v-if="chat.sendTime" class="chat-tips">
                                <span class="am-radius" style="color: #666666">{{chat.sendTime}}</span>
                            </div>
                            <article class="am-comment" :class="{ 'am-comment-flip' : chat.fd == currentUser.userFd }">
                                <a href="#link-to-user-home">
                                    <img :src="chat.avatar" alt="" class="am-comment-avatar"
                                         width="48" height="48"/>
                                </a>
                                <div class="am-comment-main">
                                    <header class="am-comment-hd">
                                        <div class="am-comment-meta">
                                            <a href="#link-to-user" class="am-comment-author">{{chat.username}}</a>
                                        </div>
                                    </header>
                                    <div class="am-comment-bd">
                                        <div class="bd-content">
                                            <template v-if="chat.type === 'text'" v-html="html_msg">
                                                {{chat.content}}
                                            </template>
                                            <template v-else-if="chat.type === 'image'">
                                                <img :src="chat.content" width="100%">
                                            </template>
                                            <template v-else v-html="html_msg">
                                                {{chat.content}}
                                            </template>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </template>
                    </template>
                </ul>
            </div>
            <div id="emoji_box" style="display: none;">

            </div>
            <div class="windows_input">
                <div class="am-btn-toolbar">
                    <div class="am-btn-group am-btn-group-xs">
                        <button type="button" class="am-btn" @click="picture"><i class="am-icon am-icon-picture-o"></i>
                        </button>
                        <input type="file" id="fileInput" style="display: none" accept="image/*">
                    </div>
                    <div class="am-btn-group am-btn-group-xs" style="width:38px;text-align: center;line-height: 28px;background: #dddddd;">
                        <img src="images/emoji.png" alt="表情" @click="emoji" style="width: 16px;height: 16px;cursor: pointer;">
                    </div>
                </div>
                <div class="input-box">
                    <label for="text-input" style="display: none"></label>
                    <textarea name="" id="text-input" cols="30" rows="10" title=""></textarea>
                </div>
                <div class="toolbar">
                    <!--                    <div class="left"><a href="http://www.easyswoole.com/" target="_blank">星辰网络博客</a>-->
                    <!--                    </div>-->
                    <div class="right">
                        <button class="send" @click="clickBtnSend">发送消息 ( Enter )</button>
                    </div>
                </div>
            </div>
        </div>
    </template>
</div>
<script>
    var Vm = new Vue({
        el        : '#chat',
        data      : {
            websocketServer  : "<?= $server ?>",
            param_data       : JSON.parse('<?= $param_data ?>'),
            websocketInstance: undefined,
            Reconnect        : false,
            ReconnectTimer   : null,
            HeartBeatTimer   : null,
            ReconnectBox     : null,
            currentUser      : {username: '-----', intro: '-----------', fd: 0, avatar: 0},
            roomChat         : [],
            up_recv_time     : 0,
        },
        created   : function () {
            this.connect();
        },
        mounted   : function () {
            var othis = this;
            var textInput = $('#text-input');
            textInput.on('keydown', function (ev) {
                if (ev.keyCode == 13 && ev.shiftKey) {
                    textInput.val(textInput.val() + "\n");
                    return false;
                } else if (ev.keyCode == 13) {
                    othis.clickBtnSend();
                    ev.preventDefault();
                    return false;
                }
            });
            $('.online-list').on('click', function () {
                $('.online_window').show();
                $('.windows_input').hide();
            });
            $('.times-icon').on('click', function () {
                $('.online_window').hide();
                $('.windows_input').show();
            });
            var input = document.getElementById("fileInput");
            input.addEventListener('change', readFile, false);

            function readFile() {
                var file = this.files[0];
                //判断是否是图片类型
                if (!/image\/\w+/.test(file.type)) {
                    alert("只能选择图片");
                    return false;
                }
                if (file.size > 1048576) {
                    alert('图片大小不能超过1MB');
                    return false;
                }
                var reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = function (e) {
                    othis.broadcastImageMessage(this.result)
                }
            }
        },
        methods   : {
            connect              : function () {
                var othis = this;
                var json_data = this.param_data;
                var username = json_data.current_username;
                var websocketServer = this.websocketServer;
                if (username) {
                    websocketServer += '?username=' + encodeURIComponent(username)+'&user_fd='+json_data.current_fd
                }
                this.websocketInstance = new WebSocket(websocketServer);
                this.websocketInstance.onopen = function (ev) {
                    // 断线重连处理
                    if (othis.ReconnectBox) {
                        layer.close(othis.ReconnectBox);
                        othis.ReconnectBox = null;
                        clearInterval(othis.ReconnectTimer);
                    }
                    // 前端循环心跳 (1min)
                    othis.HeartBeatTimer = setInterval(function () {
                        othis.websocketInstance.send('PING');
                    }, 1000 * 30);
                    // 请求获取自己的用户信息和在线列表
                    othis.release('index', 'info');
                    //othis.release('index', 'online');
                    othis.websocketInstance.onmessage = function (ev) {
                        try {
                            var data = JSON.parse(ev.data);
                            // console.log(data)
                            if (data.sendTime) {
                                if (othis.up_recv_time + 10 * 1000 > (new Date(data.sendTime)).getTime()) {
                                    othis.up_recv_time = (new Date(data.sendTime)).getTime();
                                    data.sendTime = null;
                                } else {
                                    othis.up_recv_time = (new Date(data.sendTime)).getTime();
                                }
                            }
                            switch (data.action) {
                                case 105 : {
                                    console.log(data)
                                    // 收到私聊用户消息
                                    var broadcastMsg = {
                                        type    : data.type,
                                        fd      : data.fd,
                                        content : analysis_emoji(data.content),
                                        avatar  : data.avatar,
                                        username: data.username,
                                        sendTime: data.sendTime
                                    };
                                    othis.roomChat.push(broadcastMsg);
                                    break;
                                }
                                case 201: {
                                    // 刷新自己的用户信息
                                    othis.currentUser.intro = data.content;
                                    othis.currentUser.avatar = data.avatar;
                                    othis.currentUser.fd = data.fd;
                                    othis.currentUser.username = data.username;
                                    break;
                                }
                            }
                        } catch (e) {
                            console.warn(e);
                        }
                    };
                    othis.websocketInstance.onclose = function (ev) {
                        othis.doReconnect();
                    };
                    othis.websocketInstance.onerror = function (ev) {
                        othis.doReconnect();
                    }
                }
            },
            doReconnect          : function () {
                var othis = this;
                clearInterval(othis.HeartBeatTimer);
                othis.ReconnectBox = layer.msg('已断开，正在重连...', {
                    scrollbar : false,
                    shade     : 0.3,
                    shadeClose: false,
                    time      : 0,
                    offset    : 't'
                });
                othis.ReconnectTimer = setInterval(function () {
                    othis.connect();
                }, 1000)
            },
            /**
             * 向服务器发送消息
             * @param controller 请求控制器
             * @param action 请求操作方法
             * @param params 携带参数
             */
            release              : function (controller, action, params) {
                controller = controller || 'index';
                action = action || 'action';
                params = params || {};
                var message = {controller: controller, action: action, params: params}
                this.websocketInstance.send(JSON.stringify(message))
            },
            /**
             * 发送文本消息
             * @param content
             */
            broadcastTextMessage : function (content) {
                this.release('broadcast', 'roomBroadcast', {content: content, type: 'text',tofd:this.param_data.fd,current_fd:this.param_data.current_fd})
            },
            /**
             * 发送图片消息
             * @param base64_content
             */
            broadcastImageMessage: function (base64_content) {
                this.release('broadcast', 'roomBroadcast', {content: base64_content, type: 'image',tofd:this.param_data.fd,current_fd:this.param_data.current_fd})
            },
            picture              : function () {
                var input = document.getElementById("fileInput");
                input.click();
            },
            emoji              : function () {
                // 显示表情
                var emoji_div = document.getElementById("emoji_box");

                if(emoji_div.style.display === 'block'){
                    emoji_div.style.display = 'none';
                } else {
                    emoji_div.style.display = 'block';
                    this.createEmoji();
                }
            },
            createEmoji : function(){
                // 创建表情
                var row = 8, col = 10;
                var str = '<table class="emoji">';
                for (var i = 0; i < row; i++) {
                    str += '<tr>';
                    for (var j = 0; j < col; j++) {
                        var n = i * col + j;
                        str += '<td>' + (n > 71 ? '' : ('<img onclick="select_emoji(' + n + ');" src="images/emoji/' + n + '.gif" />')) + '</td>';
                    }
                    str += '</tr>';
                }
                str += '</table>';

                $("#emoji_box").html(str);
            },
            /**
             * 点击发送按钮
             * @return void
             */
            clickBtnSend         : function () {
                var textInput = $('#text-input');
                var content = textInput.val();
                if (content.trim() !== '') {
                    if (this.websocketInstance && this.websocketInstance.readyState === 1) {
                        this.broadcastTextMessage(content);
                        textInput.val('');
                    } else {
                        layer.tips('连接已断开', '.windows_input', {
                            tips: [1, '#ff4f4f'],
                            time: 2000
                        });
                    }
                } else {
                    layer.tips('请输入消息内容', '.windows_input', {
                        tips: [1, '#3595CC'],
                        time: 2000
                    });
                }
            },
            changeName           : function () {
                layer.prompt({title: '拒绝吃瓜，秀出你的昵称', formType: 0}, function (username, index) {
                    if (username) {
                        localStorage.setItem('username', username);
                        window.location.reload();
                    }
                    layer.close(index);
                });

            }
        },
        computed  : {
            currentCount() {
                return Object.getOwnPropertyNames(this.roomUser).length - 1;
            }
        },
        directives: {
            scrollBottom: {
                componentUpdated: function (el) {
                    el.scrollTop = el.scrollHeight
                }
            }
        }
    });

    // 输入框
    var user_input = document.getElementById("text-input");

    // 选择表情
    function select_emoji(n)
    {
        cursor_insert(user_input, '{#' + n + '}');
        $("#emoji_box").fadeOut();
    }

    // 光标处插入内容
    function cursor_insert(obj, txt) {
        if (document.selection) {
            obj.selection.createRange().text = txt;
        } else {
            var v = obj.value;
            var i = obj.selectionStart;
            obj.value = v.substr(0, i) + txt + v.substr(i);
            user_input.focus();
            obj.selectionStart = i + txt.length;
        }
    }

    // 解析消息中的表情
    function analysis_emoji(str) {
        var p = /{#(\d|[1-6]\d|7[01])}/;
        if (p.test(str)) {
            return analysis_emoji(str.replace(p, "<img src='images/emoji/$1.gif'/>"))
        } else {
            return str;
        }
    }


</script>
</body>
</html>