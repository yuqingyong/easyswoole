<?php

namespace App\HttpController;

use App\Utility\ReverseProxyTools;
use EasySwoole\EasySwoole\Config;
use EasySwoole\Http\AbstractInterface\Controller;

/**
 * Class Chat
 * @package App\HttpController
 */
class Chat extends Base
{
    function index()
    {
        $request = $this->request();
        $hostName = $this->cfgValue('WEBSOCKET_HOST', 'ws://127.0.0.1:9501');
        $data = $request->getRequestParam();
        $this->render('chat', [
            'server' => $hostName,
            'param_data' => json_encode($data)
        ]);
    }
}
