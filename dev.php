<?php
/**
 * Created by PhpStorm.
 * User: yf
 * Date: 2019-01-01
 * Time: 20:06
 */

return [
    'SERVER_NAME' => "EasySwoole",
    'MAIN_SERVER' => [
        'LISTEN_ADDRESS' => '0.0.0.0',
        'PORT' => 9501,
        'SERVER_TYPE' => EASYSWOOLE_WEB_SOCKET_SERVER, //可选为 EASYSWOOLE_SERVER  EASYSWOOLE_WEB_SERVER EASYSWOOLE_WEB_SOCKET_SERVER,EASYSWOOLE_REDIS_SERVER
        'SOCK_TYPE' => SWOOLE_TCP,
        'RUN_MODEL' => SWOOLE_PROCESS,
        'SETTING' => [
            'worker_num' => 8,
            'task_worker_num' => 8,
            'reload_async' => true,
            'task_enable_coroutine' => true,
            'max_wait_time' => 5,
            'document_root' => EASYSWOOLE_ROOT . '/Static',
            'enable_static_handler' => true,
        ],
    ],
    'TEMP_DIR' => null,
    'LOG_DIR' => null,
    'CONSOLE' => [
        'ENABLE' => true,
        'LISTEN_ADDRESS' => '47.96.112.187',
        'HOST' => '47.96.112.187',
        'PORT' => 9500,
        'USER' => 'root',
        'PASSWORD' => '123456'
    ],
    'DISPLAY_ERROR' => true,
    'PHAR' => [
        'EXCLUDE' => ['.idea', 'Log', 'Temp', 'easyswoole', 'easyswoole.install']
    ],

    // 当前的域名
    'HOST' => 'http://swoole.yuqingyong.cn:9501',
    'WEBSOCKET_HOST' => 'ws://47.96.112.187:9501',


    'USERNAME' => 'mipone@foxmail.com',
    'CHECK_EMAIL' => true,
    'EMAIL_SETTING' => [
        'PORT' => 465,
        'FORM' => 'EASY-DEMO <mipone@foxmail.com>',
        'SERVER' => 'smtp.qq.com',
        'SECURE' => true,
        'PASSWORD' => 'abltlnhpmdyfbcga',
    ],
    'MYSQL'         => [
        'host'                 => '127.0.0.1',
        'port'                 => 3306,
        'user'                 => 'root',
        'password'             => 'Yqy1994.',
        'database'             => 'easyswoole',
        'timeout'              => 30,
        'charset'              => 'utf8mb4',
        'connect_timeout'      => '5',//连接超时时间
    ],
];
